# Documentation

## Engine

We spent the first two months of working on the project researching engines and choosing the right
one. We started with Unity, as that was what other teams chose, but after some deliberation we chose
Unreal Engine, as it better aligns with our preferences, idea of the scene model and we're far more
familiar with C++ than with C#. What follows is a few key points that influenced our decision making
process.

### Unity

While fairly similar to Unreal Engine, Unity has a few key differences. It uses C# instead of C++,
it features a paid subscription model called Unity Pro and isn't source-accessible. All of those
factors were what drove us to seek an alternative.

One of the biggest friction points was differentiating which part of the GameObject was hit byt a
raycasting operation. Since there can only be a single collider of each type attached to a
GameObject, the only solutions seemed to be using different colliders (which isn't possible with
more than 2-3 colliders) or splitting the object into many different GameObjects. Then it becomes
hard to keep track of all the relations between them in scripts. While we're sure there's an elegant
solution to this very common problem, we weren't able to figure one out.

Finally, the asset format support in Unity was a bit lackluster. There's no built-in support for
GLTF and integration with other tools is a bit dodgy. Contrast that to Unreal Engine's plugin for
Blender, which supports direct export including material conversion, which makes iterating models
much faster.

### Unreal Engine 4

First off, Unreal Engine comes with its so-called _Gameplay Framework_, which is a library of core
functionality for developing games. While both Unreal Engine and Unity are made as game engines,
Unreal Engine also commonly used for things like film production, meaning the game related portion
of functionality is better suited as a separate module. This also enables Unreal Engine to contain
far more assumptions about the game being designed, since it's far more flexible to pick and choose
which of those feature will be actually used.

This means that things like network multiplayer, server replication, same-screen multiplayer,
damage, animations, physics, projectiles and so on are supported out of the box.

Unlike Unity, Unreal Engine's actors can have multiple instances of the same component attached,
which means that it's trivial to support multiple colliders, multi-part meshes and reference many
components across the whole actor using the same script. Conversely, all of the actor's behaviour is
contained within the actor itself, which means that attaching scripts like in Unity isn't possible.
This functionality is instead supplemented with a bit more limited system of Actor Components.

Last but not least, since Unreal Engine isn't its proprietor's primary source of income, it and all
related resources, learning courses and documentation are completely free. Epic Games even went as
far as to make the whole Quixel Megascans library free to use for all Unreal Engine projects.
There's also a YouTube channel with hundreds of hours worth of talks, webinars and tutorials
straight from Epic Games.

### Collaboration

Unreal Engine also offers some interesting collaboration options. It's built to use Perforce as its
versioning backend, but has provisional git support as well. There's also a feature called
multi-user editing, which enables multiple users to work in the same scene at once. They'll see each
other in real-time and can see what the other one is pointing at. It's not unlike Google Docs, but
for game development.

## World

World design is the part of our project that we're the least happy with. It turned out to be a much
bigger undertaking than we anticipated, taking up much more time than was realistic in our case.
What follows is a brief account of the choices we made, the tools we used and a showcase of some
details we included in our world design.

### Art direction

We went through a couple of ideas for the aesthetic choice for our project. We ultimately settled on
a dense, vertical urban dwelling, heavily inspired by various Cyberpunk media and the Kowloon walled
city which used to exist in Hong Kong. We assembled a moodboard and wrote out some pros and cons,
which included:

 - it tends to be very cluttered, which introduces the opportunity for lots of visual interest,
 - it's conducive to asset reuse, since the objects found in such environment aren't very varied
 - it's very easy to restrict the access flow of the map, since the environment is likely to feature
   many gates, doors, etc.
 - doesn't feature a lot of open areas, meaning the requirement to model far away things is reduced.

This turned out to be a pretty good choice, since it was easy to find assets and diversify the
environment with copy and paste.

<figure>
    <img src="./moodboard.png" height="480px" />
    <figcaption>An excerpt from our moodboard</figcaption>
</figure>

### Scope

We chose a fairly limited scope for the world, which we modeled using a directed graph in advance.
Even then, the level design tasked proved to be a bit more than we could handle, since we spent a
lot of the time on mechanics and didn't have enough time to finished all the locations we wanted.

<figure>
    <img src="./world.png" height="480px" />
    <figcaption>The world graph</figcaption>
</figure>

### Assets

Finding assets to use proved to be fairly easy. Other than having access to the previously mention
Quixel Megascans library, we also utilized some permissively licensed assets from Sketchfab and our
audio needs were met by FreeSound. Some of the assets required a little remixing or tweaking, which
is where their permissive licenses came in handy.

We also created a few assets by hand using Blender and acquired some free asset packs from the
Unreal Engine marketplace during their monthly free offerings.

Overall, we didn't see the need to buy any paid assets and are happy to report that making a game
with assets found online is feasible. It's not optimal for shipping, but for development purposes
it's very easy to found usable assets as placeholders.

#### Ian Hubert

A huge source of inspiration and a big help was [Ian Hubert](https://www.youtube.com/c/mrdodobird)'s
YouTube Channel, which features his "Lazy Tutorials", a series of very brief tutorial on how to make
very quick assets using photogrammetry, texture reprojection and a few other techniques which make
it super easy to create fast realistic models. We cannot recommend this enough.

### World composition

A big help in the development process was Unreal Engine's world composition system, which enables
splitting large levels into sublevels and streaming them in using custom logic. This made it
possible to better collaborate on the project, since smaller parts of the world could be split off
and handled easily by git.

It also made it possible to explore a very large map on limited hardware resources, since the
objects are only loaded in as necessary. This does however limit the capability of actors to
reference actors found in other levels, so compound actors like the elevator need to be placed in
the persistent level.

## Moving objects

Next up was populating the world with objects the player could interact with. This proved to be the
most fun part of the project, since we could play around with mechanics and design interesting
interactions. This is where Unreal Engine's Blueprint scripting language came in very handy, since
it made it possible to iterate on ideas very fast and without leaving the editor.

### Door

The door, while seemingly simple, proved to be a very good opportunity for some in-editor tooling
creation. By exposing some variables and utilising them in the construction script, the door is
quite modular in design. It can either open away from the play or in the same fixed direction every
time (in which case that direction is signified in the editor using an Arrow component), it can be
cracked slightly open by setting the Open Amount variable, it can be locked and some text can be
placed on it by genereting a text component at construction time.

<figure>
    <img src="./door_settings.png" height="480px" />
    <figcaption>In-editor door settings</figcaption>
</figure>

### Elevator

The elevator is one of the first actors we made. It provides a handly place for the player to
collect themselves, it cleanly separates various areas, provides easy gating between places and
gives the game time to load the next level seamlessly.

The way the elevator works in the editor is that it takes an array of Floor objects, which are
placed throughout the level. It generates a button inside the cabin for every floor and when its
pressed, it goes to that floor. The only issue right now is that it directly interpolates between
the two floors, instead of going through the floor in between them. This isn't an issue for fully
vertical elevator shafts, but could be an issue for non-linear ones.

The elevator has an internal queue, which gets commands pushed into it. That way, the elevator can
handle being called from multiple places at once or pushing multiple floor buttons. It doesn't do
any sorting right now and just goes through the floors in the order they were pressed.

### Computer

The computer is possibly the most complex actor in the game, as it contains what's basically a
simple terminal emulator. It's supposed to enable some story exposition by exploring stored files --
a feature that is not implemented as of yet. It does however support influencing the world by
controlling other actors.

<figure>
    <img src="./computer.png" height="480px" />
    <figcaption>The terminal emulator</figcaption>
</figure>

The computer can also control other actors in the game, like doors or the elevator. This is done
using blueprint interfaces, similar to the interaction system. Each object can implement a list of
commands it supports and the computer will then automatically render them into the terminal
emulator.

The keyboard sound itself is somewhat interesting, since Unreal Engine has a feature called Audio
Blueprints, which enables bundling of logic into sound assets. This means that every time the Key
sound cue is played, it actually executes some logic to randomize the pitch and used sound. This
means that the control over the sound properties is contained within the sound asset and computer
doesn't have to know anything about pitch modulation, it just plays a key sound.

### Roomba

Theres' a very simple Roomba actor in the game. It doesn't contain any behaviour tree logic, it just
goes forward until it bumps into something, then rotates randomly and starts over. It was very fast
to prototype using the Blueprint system and was very fun to chase around.

### Crows

The crow is a very simple AI actor with its behaviour driven by a behaviour tree. Unreal Engine
conveniently provides a behaviour tree system, which was used for this. The crow will idly walk
around and peck on the ground, until spooked by an actor, in which case it will find a random
location and fly away. The original plan was to have them perch on locations and return to the place
later, but this was not finished due to the lack of 3D agent navigation in Unreal.

<figure>
    <img src="./crow_bt.png" height="480px" />
    <figcaption>The crow's complete behaviour tree</figcaption>
</figure>

The crow's animation comes from an animal variety pack acquired from the Unreal Marketplace. The
crow's animation itself is made from the animation blueprint, which contains extracts necessary info
from the actor and defines a state machine that transitions between on-ground and in-air states. It
also uses a 1D blendspace, which blends together the idle, walking and hopping animations based on
the actor's speed.

<figure>
    <img src="./crow_sm.png" height="480px" />
    <figcaption>Crow's AnimBP state machine</figcaption>
</figure>


## Mechanics

Lastly, let's take a look at some of the core mechanics present in the game and how they were done.

### Interaction

Interacting with the world manually -- whether by pressing buttons, picking up objects or opening
doors -- is the absolute centerpoint of the game experience. When designing this system, we were
heavily inspired by Prey (2017) and its seamless transition between interacting with the world and
interacting with computer screens.

We accomplished this in two parts. First, interacting with the world is done using a Blueprint
Interface -- an equivalent to C++ interfaces, which enables us to treat every blueprint implementing
it the same. This way, each actor could implement which parts of it are interactable, what they do
when interacted with and what they're described as when hovered with the cursor. After that, the
player class could contain all the relevant interface code and this made the concerns clearly and
nicely separated.

Second, we used the Unreal Motion Graphics toolkit to create the UI of the phone and some other
prototype objects in the world. After that, Unreal provides a pre-made component called
WidgetInteraction, which makes it easy to interact with objects placed in the world in a 3D fashion.

### Phone

The centerpiece of interaction in the game is the smartphone. It contains four sections:

 - the messenger app used for dialogue,
 - the flashlight,
 - the music player,
 - and the settings screen, which also acts like the main menu.

<figure>
    <img src="./musicapp.png" height="480px" />
    <figcaption>A screenshot of the music player in action</figcaption>
</figure>

The phone is fully 3D and can be interacted with even when laying down on a table or held in a
different position. With some tweaking, it would also be very simple to make it work in VR, where
the phone could be held in one hand and interacted with using the other.

#### Music player

Since the game was supposed to be mainly about taking in the atmosphere and exploration, we wanted
to underlay it with a selection of songs the player could listen to while wandering around the
world. This was originally done using a separate Walkman item, but was later merged into the phone
in the form of the music app.

### Dialogue

The dialogue in our game is perceived using the messenger app of the phone. We made a custom
scripting language for writing dialogue, which compiles into a CSV representation processed by
Unreal Engine at runtime. It features the ability to make decisions (using pre-made prompts which
appear in place of the keyboard), influence the real world (by triggering events) and can be timed
using delays and time-based triggers.

<figure>
    <img src="./convo.png" height="480px" />
    <figcaption>An example conversation source code.</figcaption>
</figure>

While we didn't end up writing much dialogue for the game due to time constraints, we found the tool
to be very pleasant to work with and we'd like to extend it to work with voice-acted audio as well
(by adding animation cues and so on).

#### Voice acting

We didn't end up doing any voice-acted NPCs for the game, but I feel I should metion
[Replica Studios](https://replicastudios.com/), which have a tool that can generate voiceover lines
using AI. They even have an Unreal Engine plugin, which makes it very pleasant to work with
voiceover straight in the editor.