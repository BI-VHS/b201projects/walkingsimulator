# Tech

We are using Unreal Engine and preparing objects in Blender.

# World overview

The world map has the following layout, revealing how scenes are connected.

<img width="700" alt="portfolio_view" src="Documentation/world.png">

# City square

The city square is a work in progress.

<img width="700" alt="screenshot1" src="screenshot1.png">
<img width="700" alt="screenshot2" src="screenshot2.png">
<img width="700" alt="screenshot3" src="screenshot3.png">