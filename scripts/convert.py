from sys import argv
from lark import Lark

wsscript_grammar = open('grammar.lark').read()

parser = Lark(wsscript_grammar)

def parse_to_csv(script):
    parse_tree = parser.parse(script)
    lines = []
    labels = {}
    cur_label = "Start"
    for statement in parse_tree.children:
        if statement.data == "label":
            cur_label = statement.children[0].rstrip(":")
            labels[str(cur_label)] = len(lines)
        elif statement.data == "instruction":
            instruction, = statement.children
            instr, attrs = instruction.data, instruction.children
            if instr == "say":
                text, = attrs
                delay = len(text)/25
                lines.append(["Line", text, "", delay, '', -1])
            elif instr == "dsay":
                delay, text = attrs
                lines.append(["Line", text, "", delay, '', -1])
            elif instr == "jp":
                label, = attrs
                lines[-1][-1] = str(label)
            elif instr == "simple_ask":
                text, = attrs
                lines.append(["Question", "", [[str(text), -1]], delay, '', -1])
            elif instr == "event":
                event_name, = attrs
                lines.append(["Event", "", "", 0, event_name, -1])
            elif instr == "ask":
                answers = []
                for answer in attrs:
                    text, label = answer.children
                    answers.append([str(text), label])
                lines.append(["Question", "", answers, delay, '', -1])
            elif instr == "end":
                lines.append(["Exit", '', '', 0, '', -1])
            else:
                print(f"Warning: unknown instruction: {instr}")
    
    def resolve_label(label):
        if label == -1:
            return -1
        if label not in labels:
            raise ValueError(f"Failed to resolve label {label}")
        return labels[label]+1
    
    csvtext = "---,Kind,Line,Replies,Delay,Event ID,NextLine\n"
    
    # Second pass: resolve labels, output CSV
    for i, line in enumerate(lines):
        replytext = ""
        for reply in line[2]:
            reply[-1] = resolve_label(reply[-1])
            replytext += f"(Line=\"{reply[0]}\",Next Line={reply[-1]}),"
        
        replytext = replytext.rstrip(',')
        replytext = f"\"({replytext})\""
        
        if not line[1]:
            line[1] = '""'
        
        line[-1] = resolve_label(line[-1])

        csvtext += f"Row_{i+1:03},\"{line[0]}\",{line[1]},{replytext},\"{line[3]:f}\",\"{line[4]}\",\"{line[5]}\""
        csvtext += '\n'
    
    return csvtext

def main():
    filename = argv[1]
    script = open(filename).read()
    csvtext = parse_to_csv(script)
    open(filename.split('.')[0]+'.csv', 'w').write(csvtext)

def test():
    script = open("convo.wsscript").read()

    csv = parse_to_csv(script)

    #print(parse_tree)

if __name__ == '__main__':
    #test()
    main()
